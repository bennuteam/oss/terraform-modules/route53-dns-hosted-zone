data "aws_route53_zone" "main" {
  name         = var.main_domain
  private_zone = var.main_private
}

resource "aws_route53_zone" "this" {
  count         = local.zones_count
  name          = element(local.hosted_zones, count.index)
  force_destroy = var.zone_force_destroy
}

resource "aws_route53_record" "this" {
  count   = local.zones_count
  zone_id = data.aws_route53_zone.main.zone_id
  name    = element(local.hosted_zones, count.index)
  type    = "NS"
  ttl     = var.zone_record_ttl
  records = element(aws_route53_zone.this.*.name_servers, count.index)
}
