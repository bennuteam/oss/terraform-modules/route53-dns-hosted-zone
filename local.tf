locals {
  hosted_zones = flatten([
    for prefix in var.zone_prefixes : format("%s.%s", prefix, var.main_domain)
  ])
  zone_ids          = zipmap(aws_route53_record.this.*.name, aws_route53_zone.this.*.zone_id)
  zone_name_servers = zipmap(aws_route53_record.this.*.name, aws_route53_zone.this.*.name_servers)
  zones_count       = length(local.hosted_zones)
}
