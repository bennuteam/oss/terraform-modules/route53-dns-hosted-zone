variable "main_domain" { type = string }
variable "main_private" { default = false }
variable "zone_force_destroy" { default = true }
variable "zone_prefixes" { type = list(string) }
variable "zone_record_ttl" { default = "30" }
