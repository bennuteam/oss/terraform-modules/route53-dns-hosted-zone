output "name_servers" { value = local.zone_name_servers }
output "zone_ids" { value = local.zone_ids }
output "zone_record_fqdns" { value = aws_route53_record.this.*.fqdn }
output "zone_record_names" { value = aws_route53_record.this.*.name }
